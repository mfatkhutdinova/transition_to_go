package points_machine

import (
	"errors"
	"fmt"
	"strings"
	"sync"
)

type service struct {
	accounts     map[int]uint
	transactions []*transaction

	mu sync.Mutex
}

func New() *service {
	return &service{
		accounts:     make(map[int]uint),
	}
}

func (s *service) GetState() string {
	var buf strings.Builder

	buf.WriteString("accounts:\n")
	for id, amount := range s.accounts {
		buf.WriteString(fmt.Sprintf("id=%d, amount=%d\n", id, amount))
	}

	buf.WriteString("transactions:\n")
	for _, trx := range s.transactions {
		buf.WriteString(fmt.Sprintf("id=%d, type=%d, amount=%d\n", trx.clientID, trx.transactionType, trx.amount))
	}

	return buf.String()
}

func (s *service) InitAccount(id int) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	amount, ok := s.accounts[id]
	if ok {
		return errors.New("account already exists")
	}
	s.accounts[id] = amount

	return nil
}

type TransactionType uint8

const (
	AccrualType TransactionType = iota + 1
	ChargeType
)

type transaction struct {
	clientID        int
	transactionType TransactionType
	amount          uint
}

func (s *service) Accrual(id int, amount uint) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	_, ok := s.accounts[id]
	if !ok {
		return errors.New("account does not exists")
	}
	s.accounts[id] += amount

	trx := transaction{
		clientID:        id,
		transactionType: AccrualType,
		amount:          amount,
	}
	s.transactions = append(s.transactions, &trx)

	return nil
}

func (s *service) Charge(id int, amount uint) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	accountAmount, ok := s.accounts[id]
	if !ok {
		return errors.New("account not exists")
	}
	if accountAmount < amount {
		return errors.New("no enough points")
	}
	s.accounts[id] -= amount
	trx := transaction{
		clientID:        id,
		transactionType: ChargeType,
		amount:          amount,
	}
	s.transactions = append(s.transactions, &trx)

	return nil
}
