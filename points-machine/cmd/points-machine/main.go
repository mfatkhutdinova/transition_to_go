package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	points_machine "git.ozon.dev/practice/23_transition_to_go/points-machine/internal/app/points-machine"
)

type machineInterface interface {
	InitAccount(id int) error
	Accrual(id int, amount uint) error
	Charge(id int, amount uint) error
	GetState() string
}

type baseRequest struct {
	ID     int  `json:"id"`
	Amount uint `json:"amount"`
}

func main() {
	var machine machineInterface
	machine = points_machine.New()

	http.HandleFunc("/v1/init", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		var req baseRequest
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, "cannot decode request")
			return
		}

		if err := machine.InitAccount(req.ID); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "cannot init account id = %d: %v", req.ID, err)
		}
	})

	http.HandleFunc("/v1/accrual", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		var req baseRequest
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, "cannot decode request")
			return
		}

		if  err := machine.Accrual(req.ID, req.Amount); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "cannot accrual id = %d: %v", req.ID, err)
		}
	})

	http.HandleFunc("/v1/charge", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		var req baseRequest
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, "cannot decode request")
			return
		}

		if err := machine.Charge(req.ID, req.Amount); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "cannot charge id = %d: %v", req.ID, err)
		}
	})

	http.HandleFunc("/v1/state", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		w.Write([]byte(machine.GetState()))
	})

	if err := http.ListenAndServe(":7000", nil); err != nil {
		fmt.Printf("cannot start web-server: %s\n", err.Error())
		os.Exit(1)
	}
}
