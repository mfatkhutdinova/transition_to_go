package points_machine

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Accrual(t *testing.T) {
	a := assert.New(t)

	t.Run("success accrual", func(t *testing.T) {
		userID := 1
		amount := uint(100)

		svc := New()

		err := svc.InitAccount(userID)
		a.NoError(err)

		err = svc.Accrual(userID, amount)
		a.NoError(err)

		a.Equal(amount, svc.accounts[userID])

		expTrx := &transaction{
			clientID:        userID,
			transactionType: AccrualType,
			amount:          amount,
		}
		a.Equal(expTrx, svc.transactions[0])
	})
}

func Test_Charge(t *testing.T) {
	a := assert.New(t)

	t.Run("success charge", func(t *testing.T) {
		userID := 1
		amount := uint(100)

		svc := New()

		err := svc.InitAccount(userID)
		a.NoError(err)

		err = svc.Accrual(userID, amount)
		a.NoError(err)

		a.Equal(amount, svc.accounts[userID])
		expTrx := &transaction{
			clientID:        userID,
			transactionType: AccrualType,
			amount:          amount,
		}
		a.Equal(expTrx, svc.transactions[0])

		err = svc.Charge(userID, amount)
		a.NoError(err)

		a.Equal(uint(0), svc.accounts[userID])
		expTrx = &transaction{
			clientID:        userID,
			transactionType: ChargeType,
			amount:          amount,
		}
		a.Equal(expTrx, svc.transactions[1])
	})
}
