# Балльная машинка
Веб-сервис, который реализует функции создания счёта, начисление и списание баллов.

## Запуск
### Компиляция
```bash
go build -o bin/points-machine cmd/points-machine/main.go
```
И запуск:
```bash
./bin/points-machine
```
### Интерпретация
```bash
go run cmd/points-machine/main.go
```

## Makefile
Для запуска make-целей в Makefile, нужно [скачать](https://www.gnu.org/software/make/) установить утилиту make (возможно с помощью пакетного менеджера в вашей ОС).  
Почитать про утилиту make можно [на хабре](https://habr.com/ru/post/211751/).
### Цели в Makefile
* `make build` - компилирует объектный исполняемый файл в папку `bin`
* `make run` - запускает в режиме интерпретатора сервис
* `make test` - запускает все тесты в корневой папке проекта и в подпапках
