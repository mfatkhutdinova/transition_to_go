package main

import (
	"23_transition_to_go/service_authorization/internal/app/servise_authorization"
	"encoding/json"
	"fmt"
	"net/http"
)

type serviseAuthorizationInterface interface {
	InitNumber(id int) error
	CheckNumber(id int) error
}

type baseRequest struct {
	AutoNumber int `json:"auto_number"`
}

func main() {
	var serviseAuthorization serviseAuthorizationInterface
	serviseAuthorization = servise_authorization.New()

	mux := http.NewServeMux()
	mux.HandleFunc("/v1/subscription", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		var req baseRequest
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, "Cannot decode request")
			return
		}

		if err := serviseAuthorization.InitNumber(req.AutoNumber); err != nil {
			w.WriteHeader(http.StatusConflict)
			fmt.Fprintf(w, "success - false. Cannot init servise authorization id = %d: %v", req.AutoNumber, err)
		} else {
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, "success - true")
		}
	})

	mux.HandleFunc("/v1/check", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		var req baseRequest
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, "Cannot decode request")
			return
		}

		if err := serviseAuthorization.CheckNumber(req.AutoNumber); err != nil {
			w.WriteHeader(http.StatusConflict)
			fmt.Fprint(w, "pass - false. Unauthorized car number. Please, subscribe your car")
		} else {
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, "pass - true")
		}
	})

	http.ListenAndServe(":9000", mux)
}
