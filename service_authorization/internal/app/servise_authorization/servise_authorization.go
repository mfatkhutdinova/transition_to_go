package servise_authorization

import (
	"errors"
	"sync"
)

type service struct {
	auto_number []int
	mu          sync.Mutex
}

func New() *service {
	return &service{
		auto_number: []int{},
	}
}

func (s *service) InitNumber(id int) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	exist := contains(id, s.auto_number)
	if exist {
		return errors.New("Car number already exists")
	} else {
		s.auto_number = append(s.auto_number, id)
		return nil
	}
}

func (s *service) CheckNumber(id int) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	exist := contains(id, s.auto_number)
	if exist {
		return nil
	} else {
		return errors.New("Unauthorized car number")
	}
}

func contains(id int, s []int) bool {
	for _, num := range s {
		if num == id {
			return true
		}
	}
	return false
}
